** Valve has fixed the custom unicode emoticons and color. This project is for that reason dead **

## Dota Colors

This tool is used to write colored text in the Dota 2 chat. It can be used to insert any emoticon inside a text using special unicode characters

## Motivation

This tool was created because its nice for important chat binds and its not possible to get the TI4 emoticons afterwards

## Installation

There is no Installation required. Just head over to the [**Downloads section**](https://bitbucket.org/HellGate/dotacolors/downloads) and get the newest executable.

## Contributors

You can contribute by creating pull requests. I will merge then when they fit the requirements (please use a code formatter like code maid).

## License

This project is under the MIT license. The full license can be found in the project files.
Dota 2 content and materials are trademarks and copyrights of Valve or its licensors. All rights reserved.

## Changelog

* v0.1 - Initial working version
* v0.1.1 - Added new/missing emoticons as of 10.2.2016
* v0.1.2 - Added charm emoticon previews. updated DAC emoticons