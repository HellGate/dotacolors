﻿using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DotaColors {

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        public MainWindow() {
            InitializeComponent();

            ColorList.ItemsSource = new DotaColor[] {
                new DotaColor("#fff", "Default", "6"),
                new DotaColor("#c6c7c9", "Silver", "17"),
                new DotaColor("#f60c0b", "Red", "12"),
                new DotaColor("#e1433a", "Light Red", "1C"),
                new DotaColor("#f47f0b", "Orange", "13"),
                new DotaColor("#cebb38", "Gold", "14"),
                new DotaColor("#77760c", "Olive", "10"),
                new DotaColor("#a3f77e", "Light Green", "15"),
                new DotaColor("#417c42", "Dark Green", "18"),
                new DotaColor("#1579d9", "Light Blue", "19"),
                new DotaColor("#5348d3", "Blue", "16"),
                new DotaColor("#ed8cf3", "Pink", "11"),
                new DotaColor("#630865", "Purple", "1A")
            };

            EmoticonList.ItemsSource = new DotaEmoticon[] {
                new DotaEmoticon("Emoticon_wink", "wink"),
                new DotaEmoticon("Emoticon_blush", "blush"),
                new DotaEmoticon("Emoticon_cheeky", "cheeky"),
                new DotaEmoticon("Emoticon_cool", "cool"),
                new DotaEmoticon("Emoticon_crazy", "crazy"),
                new DotaEmoticon("Emoticon_cry", "cry"),
                new DotaEmoticon("Emoticon_disapprove", "disapprove"),
                new DotaEmoticon("Emoticon_doubledamage", "doubledamage"),
                new DotaEmoticon("Emoticon_facepalm", "facepalm"),
                new DotaEmoticon("Emoticon_happytears", "happytears"),
                new DotaEmoticon("Emoticon_haste", "haste"),
                new DotaEmoticon("Emoticon_hex", "hex"),
                new DotaEmoticon("Emoticon_highfive", "highfive"),
                new DotaEmoticon("Emoticon_huh", "huh"),
                new DotaEmoticon("Emoticon_hush", "hush"),
                new DotaEmoticon("Emoticon_illusion", "illusion"),
                new DotaEmoticon("Emoticon_invisbility", "invisbility"),
                new DotaEmoticon("Emoticon_laugh", "laugh"),
                new DotaEmoticon("Emoticon_rage", "rage"),
                new DotaEmoticon("Emoticon_regeneration", "regeneration"),
                new DotaEmoticon("Emoticon_sad", "sad"),
                new DotaEmoticon("Emoticon_sick", "sick"),
                new DotaEmoticon("Emoticon_sleeping", "sleeping"),
                new DotaEmoticon("Emoticon_smile", "smile"),
                new DotaEmoticon("Emoticon_surprise", "surprise"),
                new DotaEmoticon("Emoticon_aaaah", "aaaah"),
                new DotaEmoticon("Emoticon_burn", "burn"),
                new DotaEmoticon("Emoticon_hide", "hide"),
                new DotaEmoticon("Emoticon_iceburn", "iceburn"),
                new DotaEmoticon("Emoticon_tears", "tears"),
                new DotaEmoticon("Techies_emoticon", "techies"),
                new DotaEmoticon("Emoticon_fail", "fail"),
                new DotaEmoticon("Emoticon_goodjob", "goodjob"),
                new DotaEmoticon("Emoticon_headshot", "headshot"),
                new DotaEmoticon("Emoticon_heart", "heart"),
                new DotaEmoticon("Emoticon_horse", "horse"),
                new DotaEmoticon("Emoticon_grave", "grave"),
                new DotaEmoticon("TI4_copper", "ti4copper"),
                new DotaEmoticon("TI4_bronze", "ti4bronze"),
                new DotaEmoticon("TI4_silver", "ti4silver"),
                new DotaEmoticon("TI4_gold", "ti4gold"),
                new DotaEmoticon("TI4_platinum", "ti4platinum"),
                new DotaEmoticon("TI4_diamond", "ti4diamond"),
                new DotaEmoticon("Emoticon_dac15_blush", "dac15_blush"),
                new DotaEmoticon("Emoticon_dac15_surprise", "dac15_surprise"),
                new DotaEmoticon("Emoticon_dac15_cool", "dac15_cool"),
                new DotaEmoticon("Emoticon_dac15_duel", "dac15_duel"),
                new DotaEmoticon("Emoticon_dac15_frog", "dac15_frog"),
                new DotaEmoticon("Emoticon_dac15_face", "dac15_face"),
                new DotaEmoticon("Emoticon_dac15_nosewipe", "dac15_nosewipe"),
                new DotaEmoticon("Emoticon_dac15_stab", "dac15_stab"),
                new DotaEmoticon("Emoticon_dac15_transform", "dac15_transform"),
                new DotaEmoticon("Emoticon_pup", "pup"),
                new DotaEmoticon("Emoticon_bristle", "bts_bristle"),
                new DotaEmoticon("Emoticon_godz", "bts_godz"),
                new DotaEmoticon("Emoticon_lina", "bts_lina"),
                new DotaEmoticon("Emoticon_merlini", "bts_merlini"),
                new DotaEmoticon("Emoticon_rosh", "bts_rosh"),
                new DotaEmoticon("Emoticon_cocky", "cocky"),
                new DotaEmoticon("Emoticon_devil", "devil"),
                new DotaEmoticon("Emoticon_happy", "happy"),
                new DotaEmoticon("Emoticon_thinking", "thinking"),
                new DotaEmoticon("Emoticon_tp", "tp"),
                new DotaEmoticon("Emoticon_salty", "salty"),
                new DotaEmoticon("Emoticon_angel", "angel"),
                new DotaEmoticon("Emoticon_blink", "blink"),
                new DotaEmoticon("Emoticon_snot", "snot"),
                new DotaEmoticon("Emoticon_stunned", "stunned"),
                new DotaEmoticon("Emoticon_disappear", "disappear"),
                new DotaEmoticon("Emoticon_fire", "fire"),
                new DotaEmoticon("Emoticon_bountyrune", "bountyrune"),
                new DotaEmoticon("Emoticon_troll", "troll"),
                new DotaEmoticon("Emoticon_gross", "gross"),
                new DotaEmoticon("Emoticon_ggdire", "ggdire"),
                new DotaEmoticon("Emoticon_ggradiant", "ggradiant"),
                new DotaEmoticon("Emoticon_yolo", "yolo"),
                new DotaEmoticon("Emoticon_throwgame", "throwgame"),
                new DotaEmoticon("Emoticon_aegis2015", "aegis2015"),
                new DotaEmoticon("Emoticon_eyeroll", "eyeroll"),
                new DotaEmoticon("Emoticon_charm_blush", "charm_blush"),
                new DotaEmoticon("Emoticon_charm_cheeky", "charm_cheeky"),
                new DotaEmoticon("Emoticon_charm_cool", "charm_cool"),
                new DotaEmoticon("Emoticon_charm_crazy", "charm_crazy"),
                new DotaEmoticon("Emoticon_charm_cry", "charm_cry"),
                new DotaEmoticon("Emoticon_charm_disapprove", "charm_disapprove"),
                new DotaEmoticon("Emoticon_charm_facepalm", "charm_facepalm"),
                new DotaEmoticon("Emoticon_charm_happytears", "charm_happytears"),
                new DotaEmoticon("Emoticon_charm_highfive", "charm_highfive"),
                new DotaEmoticon("Emoticon_charm_huh", "charm_huh"),
                new DotaEmoticon("Emoticon_charm_hush", "charm_hush"),
                new DotaEmoticon("Emoticon_charm_laugh", "charm_laugh"),
                new DotaEmoticon("Emoticon_charm_rage", "charm_rage"),
                new DotaEmoticon("Emoticon_charm_sad", "charm_sad"),
                new DotaEmoticon("Emoticon_charm_sick", "charm_sick"),
                new DotaEmoticon("Emoticon_charm_sleeping", "charm_sleeping"),
                new DotaEmoticon("Emoticon_charm_surprise", "charm_surprise"),
                new DotaEmoticon("Emoticon_charm_wink", "charm_wink"),
                new DotaEmoticon("Emoticon_charm_smile", "charm_smile"),
                new DotaEmoticon("Emoticon_charm_onlooker", "charm_onlooker"),
                new DotaEmoticon("Emoticon_eaglesong_2015", "eaglesong_2015"),
                new DotaEmoticon("Emoticon_wrath", "wrath"),
                new DotaEmoticon("Emoticon_snowman", "snowman"),
                new DotaEmoticon("Emoticon_healed", "healed"),
                new DotaEmoticon("Emoticon_drunk", "drunk"),
                new DotaEmoticon("Emoticon_trophy_2016", "trophy_2016"),
            };
        }

        private void EmoticonList_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            DotaEmoticon emoticon = (DotaEmoticon)(sender as ListBoxItem).DataContext;
            string addstring = "[" + emoticon.Tag + "]";
            InsertTag(addstring);
            e.Handled = true;
        }

        private void ColorList_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            DotaColor color = (DotaColor)(sender as ListBoxItem).DataContext;
            string addstring = "[" + color.Tag + "]";
            InsertTag(addstring);
            e.Handled = true;
        }

        private void InsertTag(string tag) {
            int caret = ColorText.CaretIndex;
            ColorText.Text = ColorText.Text.Insert(ColorText.CaretIndex, tag);
            ColorText.CaretIndex = caret + tag.Length;
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            MatchEvaluator myEvaluator = new MatchEvaluator(this.ReplaceTags);
            string res = Regex.Replace(ColorText.Text, @"\[(.*?)]", myEvaluator);
            Clipboard.SetText(res);
        }

        public string ReplaceTags(Match m) {
            string ma = m.Value.Replace("[", "").Replace("]", "");
            char mychar = (char)int.Parse(ma, System.Globalization.NumberStyles.AllowHexSpecifier);
            return mychar.ToString();
        }

        private void Clear_Click(object sender, RoutedEventArgs e) {
            ColorText.Text = string.Empty;
        }

        private void AboutButton_Click(object sender, RoutedEventArgs e) {
            new AboutWindow().ShowDialog();
        }
    }
}