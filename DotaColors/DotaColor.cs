﻿using System.Windows.Media;

namespace DotaColors {

    public class DotaColor {

        public Brush Brush { get; set; }

        public string Description { get; set; }

        public string Tag { get; set; }

        public DotaColor(string color, string description, string tag) {
            Brush = new SolidColorBrush((Color)ColorConverter.ConvertFromString(color));
            Description = description;
            Tag = tag;
        }
    }
}