﻿namespace DotaColors {

    public class DotaEmoticon {

        private static int curIndex = int.Parse("E000", System.Globalization.NumberStyles.HexNumber);

        public string Gif { get; set; }

        public string Description { get; set; }

        public string Tag { get; set; }

        public DotaEmoticon(string gif, string description, string tag = null) {
            curIndex++;
            if (tag == null)
                tag = curIndex.ToString("X");

            Gif = "Images/Emoticons/" + gif + ".gif";
            Description = description.Replace("_", "__"); // Accessor escape
            Tag = tag;
        }
    }
}